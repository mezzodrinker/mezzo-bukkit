package mezzo.bukkit;

import java.io.File;
import java.io.IOException;

import mezzo.bukkit.command.ICommandManager;
import mezzo.bukkit.configuration.IConfigManager;
import mezzo.bukkit.logging.IPluginLogger;
import mezzo.bukkit.net.Updater;
import mezzo.bukkit.net.Updater.UpdateType;
import mezzo.util.configuration.serialization.ConfigurationSerializer;
import mezzo.util.version.Version;

import org.bukkit.plugin.java.JavaPlugin;

/**
 * <code>BukkitPlugin</code>
 * 
 * @author mezzodrinker
 */
public abstract class BukkitPlugin extends JavaPlugin {
    protected Updater              updateChecker = null;
    protected Updater              updater        = null;

    protected static ReleaseStatus releaseStatus  = null;

    public static BukkitPlugin     singleton;

    protected BukkitPlugin(int pluginID) {
        this(pluginID, null);
    }

    protected BukkitPlugin(int pluginID, String apiKey) {
        updateChecker = new Updater(this, pluginID, UpdateType.NO_UPDATE, apiKey, false);
        updater = new Updater(this, pluginID, UpdateType.CHECK_AND_UPDATE, apiKey, false);
    }

    @Override
    public void onLoad() {
        singleton = this;

        ConfigurationSerializer.registerClass(Version.class);
    }

    @Override
    public abstract void onEnable();

    @Override
    public abstract void onDisable();

    public abstract ICommandManager commandManager();

    public abstract IConfigManager configManager();

    public abstract IPluginLogger logger();

    public abstract Version getVersion();

    @Override
    public org.bukkit.configuration.file.YamlConfiguration getConfig() {
        throw new UnsupportedOperationException();
    }

    public void save() throws IOException {}

    public void load() throws IOException {}

    public File getFile(String filename) {
        return new File(getDataFolder(), filename);
    }

    public File getFile(File file) {
        return new File(getDataFolder(), file.getPath());
    }

    public File getLogsFolder() {
        File ret = getFile("logs");

        if (!ret.exists()) {
            ret.mkdirs();
        }

        return ret;
    }

    protected static void setReleaseStatus(ReleaseStatus releaseStatus) {
        BukkitPlugin.releaseStatus = releaseStatus;
    }

    public static ReleaseStatus getReleaseStatus() {
        return releaseStatus == null ? ReleaseStatus.DEV : releaseStatus;
    }

    public static enum ReleaseStatus {
        DEV("development"), ALPHA("alpha"), BETA("beta"), RELEASE("release");

        private String name = null;

        private ReleaseStatus(String s) {
            name = s;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}
