package mezzo.bukkit.command;

import org.bukkit.command.CommandExecutor;

/**
 * <code>ICommandManager</code>
 * 
 * @author mezzodrinker
 */
public interface ICommandManager extends CommandExecutor {

}
