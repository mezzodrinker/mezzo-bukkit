package mezzo.bukkit.configuration;

import java.io.IOException;

import mezzo.util.configuration.yaml.YamlConfiguration;

/**
 * <code>IConfigManager</code>
 * 
 * @author mezzodrinker
 */
public interface IConfigManager {
    public void loadConfig() throws IOException;

    public void saveConfig() throws IOException;

    public YamlConfiguration getConfig();
}
