package mezzo.bukkit.logging;

import java.util.logging.Logger;

import mezzo.bukkit.BukkitPlugin;

/**
 * <code>BasicPluginLogger</code>
 * 
 * @author mezzodrinker
 */
public class DefaultPluginLogger implements IPluginLogger {
    protected BukkitPlugin plugin = null;
    protected Logger       logger = null;

    public DefaultPluginLogger(BukkitPlugin plugin) {
        this.plugin = plugin;
        logger = plugin.getLogger();
    }

    @Override
    public void error(Object o) {
        log(String.valueOf(o), Level.SEVERE);
    }

    @Override
    public void warning(Object o) {
        log(String.valueOf(o), Level.WARNING);
    }

    @Override
    public void info(Object o) {
        log(String.valueOf(o), Level.INFO);
    }

    @Override
    public void log(Object o) {
        log(String.valueOf(o), Level.FINEST);
    }

    @Override
    public void log(Object o, Level level) {
        if (level == null) throw new IllegalArgumentException("level can not be null");

        logger.log(level.getJavaUtilEquiv(), String.valueOf(o));
    }

    @Override
    public void log(Object o, Throwable t) {
        logger.log(Level.SEVERE.getJavaUtilEquiv(), String.valueOf(o), t);
    }

    @Override
    public void debug(Object o) {
        log("[DEBUG] " + String.valueOf(o), Level.DEBUG);
    }
}
