package mezzo.bukkit.logging;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Calendar;
import java.util.logging.Logger;

import mezzo.bukkit.BukkitPlugin;

/**
 * <code>FilePluginLogger</code>
 * 
 * @author mezzodrinker
 */
public class FilePluginLogger implements IPluginLogger {
    protected BukkitPlugin          plugin     = null;
    protected Logger                logger     = null;
    protected File                  logsFolder = null;
    protected File                  logFile    = null;
    protected PrintStream           out        = null;

    protected static final Calendar c          = Calendar.getInstance();

    public FilePluginLogger(BukkitPlugin plugin) throws IOException {
        this.plugin = plugin;
        logsFolder = plugin.getLogsFolder();
        logger = plugin.getLogger();

        logFile = new File(logsFolder, plugin.getName().toLowerCase() + "-log-latest.log");
        File log0 = logFile;
        File log1 = new File(logsFolder, plugin.getName().toLowerCase() + "-log-1.log");
        File log2 = new File(logsFolder, plugin.getName().toLowerCase() + "-log-2.log");

        if (log2.exists()) {
            log2.delete();
        }

        if (log1.exists()) {
            log1.renameTo(log2);
        }

        if (log0.exists()) {
            log0.renameTo(log1);
        } else {
            log0.createNewFile();
        }

        out = new PrintStream(logFile);
    }

    @Override
    public void error(Object o) {
        log(o, Level.SEVERE);
    }

    @Override
    public void warning(Object o) {
        log(o, Level.WARNING);
    }

    @Override
    public void info(Object o) {
        log(o, Level.INFO);
    }

    @Override
    public void log(Object o) {
        log(o, Level.FINEST);
    }

    @Override
    public void log(Object o, Level level) {
        if (level == null) throw new IllegalArgumentException("level can not be null");

        logger.log(level.getJavaUtilEquiv(), String.valueOf(o));

        c.setTimeInMillis(System.currentTimeMillis());
        String year = String.valueOf(c.get(Calendar.YEAR));
        String month = String.valueOf(c.get(Calendar.MONTH));
        String day = String.valueOf(c.get(Calendar.DAY_OF_MONTH));
        String hour = String.valueOf(c.get(Calendar.HOUR_OF_DAY));
        String minute = String.valueOf(c.get(Calendar.MINUTE));
        String second = String.valueOf(c.get(Calendar.SECOND));

        while (year.length() < 4) {
            year = "0" + year;
        }

        while (month.length() < 2) {
            month = "0" + month;
        }

        while (day.length() < 2) {
            day = "0" + day;
        }

        while (hour.length() < 2) {
            hour = "0" + hour;
        }

        while (minute.length() < 2) {
            minute = "0" + minute;
        }

        while (second.length() < 2) {
            second = "0" + second;
        }

        out.println(day + "-" + month + "-" + year + " " + hour + "." + minute + "." + second + " [" + level.toString() + "] " + String.valueOf(o));
    }

    @Override
    public void log(Object o, Throwable t) {
        if (t == null) throw new IllegalArgumentException("t can not be null");
        log(o, Level.SEVERE);

        for (Throwable cause = t; t.getCause() != null; t = t.getCause()) {
            log(cause.getClass().getName() + ": " + cause.getMessage());

            StackTraceElement[] stackTrace = cause.getStackTrace();
            for (StackTraceElement element : stackTrace) {
                log("\tat " + element.getClassName() + "." + element.getMethodName() + " (" + element.getFileName() + ":" + element.getLineNumber() + ")");
            }
        }
    }

    @Override
    public void debug(Object o) {
        log(o, Level.DEBUG);
    }
}
