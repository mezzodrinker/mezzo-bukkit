package mezzo.bukkit.logging;

/**
 * <code>IPluginLogger</code>
 * 
 * @author mezzodrinker
 */
public interface IPluginLogger {
    public void error(Object o);

    public void warning(Object o);

    public void info(Object o);

    public void log(Object o);

    public void log(Object o, Level level);

    public void log(Object o, Throwable t);

    public void debug(Object o);
}
