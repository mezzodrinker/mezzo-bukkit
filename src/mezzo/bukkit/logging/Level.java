package mezzo.bukkit.logging;

/**
 * <code>Level</code>
 * 
 * @author mezzodrinker
 */
public enum Level {
    SEVERE(1000, java.util.logging.Level.SEVERE), WARNING(900, java.util.logging.Level.WARNING), INFO(800, java.util.logging.Level.INFO), FINE(500, java.util.logging.Level.FINE), FINER(400,
            java.util.logging.Level.FINER), FINEST(300, java.util.logging.Level.FINEST), DEBUG(200, java.util.logging.Level.FINEST), CONFIG(700, java.util.logging.Level.CONFIG);

    private final int                     priority;
    private final java.util.logging.Level javaUtilEquiv;

    private Level(int priority, java.util.logging.Level javaUtilEquiv) {
        this.priority = priority;
        this.javaUtilEquiv = javaUtilEquiv;
    }

    public java.util.logging.Level getJavaUtilEquiv() {
        return javaUtilEquiv;
    }

    public int getPriority() {
        return priority;
    }
}
