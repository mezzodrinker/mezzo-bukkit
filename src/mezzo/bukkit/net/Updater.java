package mezzo.bukkit.net;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mezzo.bukkit.BukkitPlugin;
import mezzo.util.version.Version;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 * <code>Updater</code>
 * 
 * @author mezzodrinker
 */
public class Updater {
    private BukkitPlugin             plugin             = null;
    private UpdateResult             result             = null;
    private UpdateType               type               = null;
    private Throwable                lastError          = null;
    private List<Version>            compatibleVersions = null;
    private Version                  version            = null;
    private URL                      fileLink           = null;
    private URL                      rss                = null;
    private File                     updateFolder       = null;
    private Thread                   updateThread       = null;
    private String                   title              = null;
    private String                   releaseType        = null;
    private String                   apiKey             = null;
    private int                      pluginID           = 0;

    private static final String      HOST               = "https://api.curseforge.com";
    private static final String      QUERY              = "/servermods/files?projectIds=";
    private static final String      PREFIX             = "[UPDATER] ";
    private static final int         BYTE_SIZE          = 1024;

    public static final List<String> IGNORED_TAGS       = Arrays.asList("-pre", "-dev", "-snapshot");
    public static final String       JSON_TITLE         = "name";
    public static final String       JSON_LINK          = "downloadUrl";
    public static final String       JSON_RELEASETYPE   = "releaseType";

    public Updater(BukkitPlugin plugin, int pluginID, UpdateType type) {
        this(plugin, pluginID, type, false);
    }

    public Updater(BukkitPlugin plugin, int pluginID, UpdateType type, boolean runNow) {
        this(plugin, pluginID, type, null, runNow);
    }

    public Updater(BukkitPlugin plugin, int pluginID, UpdateType type, String apiKey, boolean runNow) {
        if (plugin == null) throw new IllegalArgumentException("plugin can not be null");

        this.plugin = plugin;
        this.pluginID = pluginID;
        this.type = type == null ? UpdateType.NO_UPDATE : type;
        this.apiKey = apiKey;
        updateFolder = plugin.getServer().getUpdateFolderFile();

        String rssURL = HOST + QUERY + pluginID;
        try {
            rss = new URL(rssURL);
        } catch (MalformedURLException e) {
            plugin.logger().log(PREFIX + "Could not check for remote version: invalid RSS URL '" + rssURL + "'", e);
            plugin.logger().error(PREFIX + "Please report this to this plugin's author(s) and provide the line above.");
            result = UpdateResult.ERROR_INVALIDID;
            lastError = e;
        }

        updateThread = new Thread(new UpdateRunnable());
        updateThread.setName(plugin.getName() + " " + (type == UpdateType.NO_UPDATE ? "version checker" : "updater") + "@" + Integer.toHexString(hashCode()));

        if (runNow) {
            check();
        }
    }

    protected final void waitForThread() {
        if (updateThread.isAlive()) {
            try {
                updateThread.join();
            } catch (InterruptedException e) {
                plugin.logger().warning(PREFIX + "interrupted while waiting for thread '" + updateThread.getName() + "'");
                plugin.logger().log("", e);
            }
        }
    }

    protected boolean read() {
        try {
            plugin.logger().debug(PREFIX + "opening a connection to " + rss.toString());
            URLConnection connection = rss.openConnection();
            connection.setConnectTimeout(5000);
            if (apiKey != null) {
                plugin.logger().debug(PREFIX + "using apiKey " + apiKey);
                connection.addRequestProperty("X-API-Key", apiKey);
            }
            connection.addRequestProperty("User-Agent", "Updater");
            connection.setDoOutput(true);

            plugin.logger().debug(PREFIX + "reading from input stream");
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String response = in.readLine();
            plugin.logger().debug(PREFIX + "parsing JSON array");
            JSONArray array = (JSONArray) JSONValue.parse(response);
            if (array.size() <= 0) {
                plugin.logger().error(PREFIX + "Could not check for remote version: could not find any files for plugin ID '" + pluginID + "'");
                plugin.logger().error(PREFIX + "Please report this to this plugin's author(s) and provide the lines above.");
                result = UpdateResult.ERROR_INVALIDID;
                return false;
            }

            Version bukkitVersion = Version.valueOf(plugin.getServer().getBukkitVersion().split("-")[0].replaceAll("[^0-9\\.]", ""));
            plugin.logger().debug(PREFIX + "local bukkit version is " + bukkitVersion);
            try {
                List<JSONEntry> list = JSONEntry.parse(array);
                Version version = null;
                JSONEntry entry = null;

                plugin.logger().debug(PREFIX + "iterating over versions list");
                for (JSONEntry e : list) {
                    String[] s = e.getTitle().split("(\\.jar|\\.zip)")[0].split("\\s");
                    Version v = Version.valueOf(s[s.length - 1].replaceAll("(\\[|\\])", ""));

                    plugin.logger().debug(PREFIX + "found version '" + v + "' for Bukkit " + Arrays.toString(e.getCompatibleVersions().toArray()));
                    if (!e.getCompatibleVersions().contains(bukkitVersion)) {
                        plugin.logger().debug(PREFIX + v + " is for " + Arrays.toString(e.getCompatibleVersions().toArray()) + " but not the local bukkit version");
                        continue;
                    }
                    if (version == null || version.compareTo(v) < 0) {
                        plugin.logger().debug(PREFIX + "version " + v + " is greater than the latest version " + version + ", setting latest version to " + v);
                        version = v;
                        entry = e;
                    }
                }

                plugin.logger().debug(PREFIX + "saving remote file's details");
                title = entry.getTitle();
                plugin.logger().debug(PREFIX + "title is '" + title + "'");
                fileLink = entry.getFileLink();
                plugin.logger().debug(PREFIX + "file link is '" + fileLink + "'");
                releaseType = entry.getReleaseType();
                plugin.logger().debug(PREFIX + "release type is '" + releaseType + "'");
                compatibleVersions = entry.getCompatibleVersions();
                plugin.logger().debug(PREFIX + "compatible versions are '" + Arrays.toString(compatibleVersions.toArray()) + "'");
                this.version = version;
                plugin.logger().debug(PREFIX + "version is '" + version + "'");
            } catch (IllegalArgumentException e) {
                plugin.logger().log(PREFIX + "Could not check for remote version: invalid compatible versions list", e);
                plugin.logger().error(PREFIX + "Please report this to this plugin's author(s) and provide the lines above.");
                result = UpdateResult.ERROR_INVALIDVERSION;
                lastError = e;
                return false;
            } catch (NullPointerException e) {
                plugin.logger().log(PREFIX + "Could not check for remote version: could not find any versions for Bukkit version '" + bukkitVersion + "'", e);
                plugin.logger().error(PREFIX + "Please report this to this plugin's author(s) and provide the lines above.");
                result = UpdateResult.ERROR_INVALIDVERSION;
                lastError = e;
                return false;
            } catch (ArrayIndexOutOfBoundsException e) {
                plugin.logger().log(PREFIX + "Could not check for remote version: an unexpected error occurred", e);
                result = UpdateResult.ERROR;
                lastError = e;
                return false;
            }
            return true;
        } catch (IOException e) {
            if (e.getMessage().contains("HTTP response code: 403")) {
                plugin.logger().log(PREFIX + "Could not read RSS: API key was rejected", e);
                plugin.logger().error(PREFIX + "Please report this to this plugin's author(s) and provide the lines above.");
                plugin.logger().info(PREFIX + "If this plugin provides customization of the API key, double-check your configuration to ensure your key correct.");
                result = UpdateResult.ERROR_APIKEY;
                lastError = e;
            } else {
                plugin.logger().log(PREFIX + "Could not read RSS: requested site '" + rss.toString() + "' is either offline or does not exist");
                plugin.logger().error(PREFIX + "Please report this to this plugin's author(s) and provide the lines above.");
                result = UpdateResult.ERROR_NOCONNECTION;
                lastError = e;
            }
            return false;
        }
    }

    protected boolean needsUpdate(String version) {
        return needsUpdate(Version.valueOf(version));
    }

    protected boolean needsUpdate(Version version) {
        if (type != UpdateType.NO_CHECK) {
            Version bukkitVersion = Version.valueOf(plugin.getServer().getBukkitVersion().split("-")[0].replaceAll("[^0-9\\.]", ""));
            boolean found = false;
            for (Version compatibleVersion : compatibleVersions) {
                plugin.logger().debug(PREFIX + "local bukkit version is '" + bukkitVersion + "', '" + compatibleVersion + "' is compatible");
                if (compatibleVersion.equals(bukkitVersion)) {
                    found = true;
                    plugin.logger().debug(PREFIX + "detected update for bukkit version '" + bukkitVersion + "'");
                    plugin.logger().debug(PREFIX + "local plugin version is '" + plugin.getVersion() + "', '" + this.version + "' was detected as the newest version");
                    if (IGNORED_TAGS.contains(this.version.getSuffix().toLowerCase())) {
                        plugin.logger().debug(PREFIX + "remote version contains '" + version.getSuffix() + "' and thus was ignored");
                        result = UpdateResult.UPDATE_IGNORED;
                        return false;
                    }
                    if (this.version.compareTo(plugin.getVersion()) <= 0) {
                        plugin.logger().debug(PREFIX + "plugin is up to date");
                        result = UpdateResult.NO_UPDATE;
                        return false;
                    }
                }
            }
            if (!found) {
                plugin.logger().debug(PREFIX + "latest version is not compatible with local bukkit version");
                result = UpdateResult.UPDATE_IGNORED;
                return false;
            }
        }

        return true;
    }

    protected void save(File folder, String filename, URL downloadURL) {
        plugin.logger().debug(PREFIX + "downloading update");
        if (!folder.exists()) {
            folder.mkdirs();
        }

        try (BufferedInputStream in = new BufferedInputStream(downloadURL.openStream()); FileOutputStream out = new FileOutputStream(folder.getAbsolutePath() + File.pathSeparator + filename)) {
            byte[] data = new byte[BYTE_SIZE];
            long fileLength = downloadURL.openConnection().getContentLengthLong();
            long downloaded = 0;
            int counter = 0;

            plugin.logger().debug(PREFIX + "starting download");
            while ((counter = in.read(data, 0, BYTE_SIZE)) != -1) {
                out.write(data, 0, counter);
                downloaded += counter;

                int percentage = (int) ((downloaded * 100) / fileLength);
                if (percentage % 10 == 0) {
                    plugin.logger().debug(PREFIX + "download " + percentage + "% finished");
                }
            }
            plugin.logger().debug(PREFIX + "download finished");

            if (downloadURL.getFile().endsWith(".zip")) {
                plugin.logger().debug(PREFIX + "received .zip file, extracting");
                plugin.logger().debug(PREFIX + "=== feature has not been implemented yet ==="); // TODO unzipping
            }

            plugin.logger().debug(PREFIX + "update successful");
            result = UpdateResult.SUCCESS;
        } catch (Throwable t) {
            plugin.logger().log(PREFIX + "Could not download update", t);
            result = UpdateResult.ERROR_DOWNLOAD;
        }

        plugin.logger().debug(PREFIX + "update complete");
    }

    public UpdateResult getResult() {
        waitForThread();
        return result;
    }

    public Throwable getLastError() {
        waitForThread();
        return lastError;
    }

    public String getReleaseType() {
        waitForThread();
        return releaseType;
    }

    public List<Version> getCompatibleVersions() {
        waitForThread();
        return compatibleVersions;
    }

    public Version getVersion() {
        waitForThread();
        return version;
    }

    public String getTitle() {
        waitForThread();
        return title;
    }

    public URL getFileLink() {
        waitForThread();
        return fileLink;
    }

    public UpdateResult check() {
        if (!updateThread.isAlive()) {
            updateThread.start();
        }

        return getResult();
    }

    protected static class JSONEntry {
        private String        title              = null;
        private URL           fileLink           = null;
        private String        releaseType        = null;
        private List<Version> compatibleVersions = null;

        public JSONEntry(String title, URL fileLink, String releaseType, List<Version> compatibleVersions) {
            this.title = title;
            this.fileLink = fileLink;
            this.releaseType = releaseType;
            this.compatibleVersions = compatibleVersions;
        }

        public String getTitle() {
            return title;
        }

        public URL getFileLink() {
            return fileLink;
        }

        public String getReleaseType() {
            return releaseType;
        }

        public List<Version> getCompatibleVersions() {
            return compatibleVersions;
        }

        public static List<JSONEntry> parse(JSONArray array) {
            List<JSONEntry> list = new ArrayList<JSONEntry>();

            for (int i = 0; i < array.size(); i++) {
                try {
                    List<Version> compatibleVersions = new ArrayList<Version>();
                    JSONObject current = (JSONObject) array.get(i);
                    URL fileLink = new URL((String) current.get(JSON_LINK));
                    String title = (String) current.get(JSON_TITLE);
                    String releaseType = (String) current.get(JSON_RELEASETYPE);
                    String versions = "";
                    try {
                        versions = title.split("\\[")[1].split("\\]")[0];
                    } catch (ArrayIndexOutOfBoundsException e) {
                        continue;
                    }
                    String[] elements = versions.split(",");
                    for (String element : elements) {
                        compatibleVersions.add(Version.valueOf(element));
                    }
                    list.add(new JSONEntry(title, fileLink, releaseType, compatibleVersions));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }

            return list;
        }
    }

    protected class UpdateRunnable implements Runnable {
        @Override
        public void run() {
            if (rss != null) {
                plugin.logger().debug(PREFIX + (type == UpdateType.NO_UPDATE ? "version check" : "update process") + " started");
                plugin.logger().debug(PREFIX + "reading remote version");
                if (read()) {
                    plugin.logger().debug(PREFIX + "checking if local version is out of date");
                    if (needsUpdate(version)) {
                        plugin.logger().debug(PREFIX + "local version is out of date");
                        plugin.logger().debug(PREFIX + (type == UpdateType.NO_UPDATE ? "update is available" : "downloading update"));
                        if (fileLink != null && type != UpdateType.NO_UPDATE) {
                            save(new File(plugin.getDataFolder().getParent(), updateFolder.getPath()), plugin.getName() + "-" + version + "-universal.jar", fileLink);
                        } else {
                            result = UpdateResult.UPDATE_AVAILABLE;
                        }
                    }
                }
                plugin.logger().debug(PREFIX + "done");
            }
        }
    }

    public static enum UpdateResult {
        /** An update was detected but not downloaded. */
        UPDATE_AVAILABLE,
        /** An update was detected and downloaded. */
        SUCCESS,
        /** No update was detected. */
        NO_UPDATE,
        /** An update was detected but was ignored. */
        UPDATE_IGNORED,
        /** An update was detected but an error occurred while downloading the new version */
        ERROR_DOWNLOAD(true),
        /** The updater was unable to connect to dev.bukit.org */
        ERROR_NOCONNECTION(true),
        /** The plugin's Curse ID was invalid. */
        ERROR_INVALIDID(true),
        /** The API key wasn't configured correctly. */
        ERROR_APIKEY(true), ERROR_INVALIDVERSION(true), ERROR(true);

        private boolean isError = false;

        private UpdateResult() {}

        private UpdateResult(boolean isError) {
            this.isError = isError;
        }

        public boolean isError() {
            return isError;
        }
    }

    public static enum UpdateType {
        CHECK_AND_UPDATE, NO_CHECK, NO_UPDATE;
    }
}
