package mezzo.bukkit.util;

import org.bukkit.Server;
import org.bukkit.plugin.Plugin;

public class PluginUtil {
    public static <T extends Plugin> T getPlugin(Server server, String id, Class<T> pluginClass) throws PluginNotFoundException {
        Plugin p = server.getPluginManager().getPlugin(id);
        if (p == null || !pluginClass.isInstance(p)) throw new PluginNotFoundException("server has no plugin with id " + id + " loaded");
        return pluginClass.cast(p);
    }

    public static class PluginNotFoundException extends Exception {
        private static final long serialVersionUID = -5315261631948410810L;

        public PluginNotFoundException() {
            super();
        }

        public PluginNotFoundException(String s) {
            super(s);
        }
    }
}
