package mezzo.bukkit.util;

import java.util.Map;

/**
 * <code>SerializationHelper</code>
 * 
 * @author mezzodrinker
 */
class SerializationHelper {
    private SerializationHelper() {}

    public static double asDouble(Map<String, Object> map, String key) {
        Object value = map.get(key);
        if (value == null) throw new IllegalArgumentException("missing obligatory key " + key);
        if (value instanceof String) {
            try {
                return Double.valueOf((String) value);
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException(key + "(" + value + ") is not a double");
            }
        }
        if (!(value instanceof Number)) throw new IllegalArgumentException(key + "(" + value + ") is not a number");
        return ((Number) value).doubleValue();
    }
}
