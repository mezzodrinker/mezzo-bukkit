package mezzo.bukkit.util;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.NumberConversions;

/**
 * <code>Vector</code>
 * 
 * @author mezzodrinker
 */
public abstract class Vector extends mezzo.util.math.Vector implements Cloneable {
    protected static final double epsilon = 0.000001d;

    public int getBlockX() {
        return NumberConversions.floor(getX());
    }

    public int getBlockY() {
        return NumberConversions.floor(getY());
    }

    public int getBlockZ() {
        return NumberConversions.floor(getZ());
    }

    @Override
    public abstract double getX();

    @Override
    public abstract double getY();

    @Override
    public abstract double getZ();

    @Override
    public abstract Vector add(mezzo.util.math.Vector v);

    @Override
    public abstract Vector subtract(mezzo.util.math.Vector v);

    @Override
    public abstract Vector multiply(mezzo.util.math.Vector v);

    @Override
    public abstract Vector divide(mezzo.util.math.Vector v);

    @Override
    public abstract Vector multiply(double d);

    @Override
    public abstract Vector divide(double d);

    @Override
    public abstract Vector cross(mezzo.util.math.Vector v);

    @Override
    public abstract double scalar(mezzo.util.math.Vector v);

    @Override
    public abstract double abs();

    @Override
    public abstract Vector normalize();

    public abstract Location toLocation(World world);

    @Override
    public abstract boolean equals(Object o);

    @Override
    public abstract Vector clone();
}
