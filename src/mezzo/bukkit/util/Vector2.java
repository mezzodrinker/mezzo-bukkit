package mezzo.bukkit.util;

import static mezzo.bukkit.util.SerializationHelper.asDouble;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import mezzo.util.configuration.serialization.ConfigurationSerializable;
import mezzo.util.configuration.serialization.ConfigurationSerializer;
import mezzo.util.configuration.serialization.SerializeAs;

import org.bukkit.Location;
import org.bukkit.World;

/**
 * <code>Vector2</code> - two-dimensional vectors
 * 
 * @author mezzodrinker
 */
@SerializeAs("Vector2")
public class Vector2 extends Vector implements ConfigurationSerializable {
    public final double           x;
    public final double           y;

    protected static final String SERIALIZE_X = "x";
    protected static final String SERIALIZE_Y = "y";

    static {
        ConfigurationSerializer.registerClass(Vector2.class);
    }

    public Vector2(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public double getZ() {
        return 0;
    }

    @Override
    public Vector2 divide(double a) {
        double x = this.x / a;
        double y = this.y / a;
        return new Vector2(x, y);
    }

    @Override
    public Vector2 multiply(double a) {
        double x = this.x * a;
        double y = this.y * a;
        return new Vector2(x, y);
    }

    @Override
    public Vector2 add(mezzo.util.math.Vector v) {
        double x = this.x + v.getX();
        double y = this.y + v.getY();
        return new Vector2(x, y);
    }

    @Override
    public Vector2 subtract(mezzo.util.math.Vector v) {
        double x = this.x - v.getX();
        double y = this.y - v.getY();
        return new Vector2(x, y);
    }

    @Override
    public Vector2 multiply(mezzo.util.math.Vector v) {
        double x = this.x * v.getX();
        double y = this.y * v.getY();
        return new Vector2(x, y);
    }

    @Override
    public Vector2 divide(mezzo.util.math.Vector v) {
        double x = this.x / v.getX();
        double y = this.y / v.getY();
        return new Vector2(x, y);
    }

    @Override
    public Vector2 cross(mezzo.util.math.Vector v) {
        double x = this.x * v.getY() - v.getX() * y;
        double y = this.y * v.getX() - v.getY() * this.x;
        return new Vector2(x, y);
    }

    @Override
    public double scalar(mezzo.util.math.Vector v) {
        return x * v.getX() + y * v.getY();
    }

    @Override
    public double abs() {
        return Math.sqrt(scalar(this));
    }

    @Override
    public Vector normalize() {
        return divide(abs());
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new LinkedHashMap<String, Object>();
        map.put(SERIALIZE_X, x);
        map.put(SERIALIZE_Y, y);
        return Collections.unmodifiableMap(map);
    }

    @Override
    public Location toLocation(World world) {
        return new Location(world, x, 0, y);
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Vector2)) return false;
        Vector2 v = (Vector2) o;
        return Math.abs(x - v.x) < epsilon && Math.abs(y - v.y) < epsilon;
    }

    @Override
    public Vector2 clone() {
        return new Vector2(x, y);
    }

    public static Vector2 valueOf(Location location) {
        return new Vector2(location.getX(), location.getZ());
    }

    public static Vector2 deserialize(Map<String, Object> map) {
        double x = asDouble(map, SERIALIZE_X);
        double y = asDouble(map, SERIALIZE_Y);
        return new Vector2(x, y);
    }
}
